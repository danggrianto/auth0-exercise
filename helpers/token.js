var request = require("request");
var rp = require('request-promise')

const client_id = process.env.CLIENT_ID
const client_secret = process.env.CLIENT_SECRET
const audience = process.env.AUDIENCE

function getAccessToken() {
  var options = {
    method: 'POST',
    url: process.env.TOKEN_URL,
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    json: true,
    form: {
      grant_type: 'client_credentials',
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      audience: process.env.AUDIENCE
    }
  }
  return rp(options)
}

module.exports = {
  getAccessToken
}
