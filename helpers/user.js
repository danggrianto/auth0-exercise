var request = require("request");
var rp = require('request-promise')

const API_URL = process.env.API_URL
var options = {
  method: 'GET',
  url: API_URL,
  json: true,
  headers: {authorization: ''}
};

function setToken(token){
  options.headers.authorization = 'Bearer ' + token
}

// List all users
function getUsers() {
  var opt = {...options}
  opt.url = API_URL + 'users'
  opt.qs = { search_engine: 'v3'}
  return rp(opt)
}

// Get user by id
function getUser(userId){
  var opt = {...options}
  opt.url = API_URL + 'users/' + userId
  return rp(opt)
}

// Creating user
function createUser(user){
  var opt = {...options}
  opt.method = 'POST'
  opt.url = API_URL + 'users'
  opt.body = user
  return rp(opt)
}

// Deleting user
function deleteUser(user_id){
  var opt = {...options}
  opt.method = 'DELETE'
  opt.url = API_URL + 'users/' + user_id
  return rp(opt)
}

function getUsersByEmail(userEmail) {
  var opt = {...options}
  opt.url = API_URL + 'users-by-email'
  opt.qs = {email: userEmail}
  return rp(opt)
}

module.exports = {
  getUsers,
  getUser,
  getUsersByEmail,
  deleteUser,
  setToken,
  createUser
}
