FROM node:13-alpine

WORKDIR /src

COPY package.json ./

RUN npm install

COPY . .

CMD npm test
