var user = require('../helpers/user')
var token = require('../helpers/token')
var faker = require('faker')

var profile = {}
const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

beforeAll(()=> {
  return token.getAccessToken().then(data => {
    user.setToken(data.access_token)
  })
})
// Create user profile before each test
beforeEach(()=> {
  // using faker to generate random profile
  profile = {
    "email_verified": true,
    "connection": "Username-Password-Authentication",
    "password": "helloworld",
    "email": faker.internet.email(),
    "name": faker.name.findName(),
    "nickname": faker.name.firstName(),
    "picture": faker.image.avatar()
  }
  return user.createUser(profile).then(
    response => {
      profile = response
      sleep(500)
    }
  )
})

// Delete user after each test
afterEach(()=> {
  return user.deleteUser(profile.user_id)
})

it('should be able to get list of users', () => {
  return user.getUsers().then(
    data => expect(data.length).toBeGreaterThan(0)
  )
})

/* Test find users by email */

it('should be able to find user by VALID email', ()=> {
  return user.getUsersByEmail(profile.email).then(
    data => expect(data).toHaveLength(1)
  )
})

it('should not be able to find user by INVALID email', ()=> {
  return user.getUsersByEmail('hello@email.com').then(
    data => expect(data).toHaveLength(0)
  )
})

it('should validate email query', ()=> {
  return expect(user.getUsersByEmail('hello')).rejects.toThrow('400')
})

/* Test find user by ID */
it('should be able to find user by ID', ()=> {
  return user.getUser(profile.user_id).then(
    data => expect(data.email).toBe(profile.email)
  )
})

it('should validate user_id query', ()=> {
  return expect(user.getUser('hello')).rejects.toThrow('400')
})
