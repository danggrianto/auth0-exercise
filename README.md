# AUTH0 Exercise

## Environment Variable

Before running the test make sure to setup environment variable for

- `API_URL`: URL for the API that you want to test
- `TOKEN_URL`: URL for getting the token
- `CLIENT_ID`: Client ID
- `CLIENT_SECRET`: Client Secret
- `AUDIENCE`: Audience

```
export ACCESS_TOKEN=your-access-token
export API_URL=https://sunday.auth0.com/api/v2/
```

## Building Docker Image
```
docker build -t auth0-exercise:latest .
```

## Running Test

### Using Docker (Preffered)
```
docker run -e TOKEN_URL=$TOKEN_URL -e CLIENT_ID=$CLIENT_ID -e
CLIENT_SECRET=$CLIENT_SECRET -e AUDIENCE=$AUDIENCE -e API_URL=$API_URL auth0-exercise:latest
```

### Using yarn
```
yarn run test
```

### On bitbucket
Simply go to this link and run the pipeline
https://bitbucket.org/danggrianto/auth0-exercise/addon/pipelines/home#!/

## Writing the Test

This test framework is using [Jest](https://jestjs.io/). Tests code should be
written in the directory `__tests__`.
`helpers` directory containing handlers to the endpoint. All endpoint call
should be done by this module and there should not be any API call in the tests
code.

### Test Data

Before test run, data should be clean. A random user will be created before each
test and will be deleted after test completed.


## To Do

- Adding 'failing' tests
- Not using REST to get token, probably using the SDK
- Not using REST to create/delete user, insert to DB directly
- Parallel testing to make tests faster
